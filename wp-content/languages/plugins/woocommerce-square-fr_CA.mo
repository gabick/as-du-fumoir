��          �       <      <  ;   =  :   y  M   �            	        "     .     ?     O     d     r     �     �  %   �     �     �     �  8   �  D     q   \     �     �     �     �          '     8     R     b          �  +   �     �  	   �   A product attribute with the provided ID could not be found A product category with the provided ID could not be found Basic Product information will be synced, excluding Categories and Inventory. Business Location CVV Card Code Card Number Card Postal Code Create Customer Credit card (Square) Customer Data Customer created on Square: %s Expiry (MM/YY) Guest Pay with your credit card via Square. Products Regular Project-Id-Version: WooCommerce Square 1.0.37
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce-square/issues
POT-Creation-Date: 2019-04-16 14:35:33+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-05-01 02:19+0000
Last-Translator: Gabick Gabick <admin@gabick.com>
Language-Team: Français du Canada
X-Generator: Loco https://localise.biz/
Language: fr_CA
Plural-Forms: nplurals=2; plural=n > 1;
X-Loco-Version: 2.2.2; wp-5.1.1 Un attribut de produit avec l'ID fourni est introuvable. Une catégorie de produit avec l'ID fourni n'a pas pu être trouvée Les informations de base sur le produit seront synchronisées, à l'exclusion des catégories et de l'inventaire. Adresse de l'entreprise CVC Code de la carte Numéro de la carte Code postal de la carte Créer un client Carte de crédit (Square) Données client Client créé sur Square: %s Date d'expiration (MM / AA) Invité Payez avec votre cate de crédit via Square Produits Regullier 