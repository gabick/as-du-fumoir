<?php
require_once( 'code/services-shortcode.php' );
require_once( 'code/widget/social-widget.php' );
require_once( 'code/widget/promo-banner.php' );
require_once( 'code/added-social-icons.php' );
require_once( 'code/woocommerce-hook.php' );
require_once( 'includes/admin-login/admin-login.php' );
function my_enqueue_assets() {
    wp_enqueue_style( 'parent-main', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'nwayo-style-main', '/pub/build/styles/main.css' );
    wp_enqueue_style( 'nwayo-style-footer', '/pub/build/styles/footer.css' );
    wp_enqueue_style( 'nwayo-style-copyright', '/pub/build/styles/copyright.css' );
    wp_enqueue_style( 'nwayo-style-blog', '/pub/build/styles/blog.css' );
    wp_enqueue_style( 'nwayo-style-event-calendar', '/pub/build/styles/eventcalendar.css' );
    wp_enqueue_style( 'nwayo-style-promobar', '/pub/build/styles/promobar.css' );
    wp_enqueue_style( 'nwayo-style-woocommerce', '/pub/build/styles/woocommerce.css' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' );

function my_scripts_method() {
    wp_enqueue_script(
        'nwayo-dependencies',
        '/pub/build/scripts/dependencies-head-sync.js',
        array( 'jquery' ),
        true
    );
    wp_enqueue_script(
        'nwayo-dependencies-sync',
        '/pub/build/scripts/dependencies.js',
        array( 'jquery' ),
        true
    );
    wp_enqueue_script(
        'nwayo-main',
        '/pub/build/scripts/main.js',
        array( 'jquery' ),
        true
    );
    wp_enqueue_script(
        'nwayo-woocommerce',
        '/pub/build/scripts/woocommerce.js',
        array( 'jquery' ),
        true
    );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method', 15 );

function ds_translate_text($translated) {
    $translated = str_ireplace('read more', 'Lire la suite', $translated);
    $translated = str_ireplace('« Older Entries', '< Articles plus anciens', $translated);
    $translated = str_ireplace('Next Entries »', 'Article suivant >', $translated);
    return $translated;
}
add_filter('gettext', 'ds_translate_text');
add_filter('ngettext', 'ds_translate_text');


// create new column in et_pb_layout screen
add_filter( 'manage_et_pb_layout_posts_columns', 'ds_create_shortcode_column', 5 );
add_action( 'manage_et_pb_layout_posts_custom_column', 'ds_shortcode_content', 5, 2 );
// register new shortcode
add_shortcode('ds_layout_sc', 'ds_shortcode_mod');

// New Admin Column
function ds_create_shortcode_column( $columns ) {
    $columns['ds_shortcode_id'] = 'Module Shortcode';
    return $columns;
}

//Display Shortcode
function ds_shortcode_content( $column, $id ) {
    if( 'ds_shortcode_id' == $column ) {
        ?>
        <p>[ds_layout_sc id="<?php echo $id ?>"]</p>
        <?php
    }
}
// Create New Shortcode
function ds_shortcode_mod($ds_mod_id) {
    extract(shortcode_atts(array('id' =>'*'),$ds_mod_id));
    return do_shortcode('[et_pb_section global_module="'.$id.'"][/et_pb_section]');
}

/*================================================
#Load custom Blog Module
================================================*/

//function divi_child_theme_setup() {
//    if ( ! class_exists('ET_Builder_Module') ) {
//        return;
//    }
//
//    get_template_part( 'code/builder/Blog' );
//    $gabickBlog = new Gabick_ET_Builder_Module_Blog();
//    remove_shortcode( 'et_pb_blog' );
//    add_shortcode( 'et_pb_blog', array($gabickBlog, '_shortcode_callback') );
//
//}
//
//add_action( 'wp', 'divi_child_theme_setup', 9999 );

add_filter('wc_square_is_3d_secure_enabled', '__return_false');

/**
 * Disable the emoji's
 */
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }

    return $urls;
}
?>

