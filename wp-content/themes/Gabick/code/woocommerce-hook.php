<?php
    add_filter('loop_shop_columns', 'loop_columns', 999);
    /**
     * Change number or products per row to 4
     */
    if (!function_exists('loop_columns')) {
        function loop_columns() {
            return 4; // 3 products per row
        }
    }
//    add_filter( 'woocommerce_get_breadcrumb', function($crumbs, $Breadcrumb){
//        $shop_page_id = wc_get_page_id('shop'); //Get the shop page ID
//        if($shop_page_id > 0 && !is_shop()) { //Check we got an ID (shop page is set). Added check for is_shop to prevent Home / Shop / Shop as suggested in comments
//            $new_breadcrumb = [
//                __( 'Boutique', 'woocommerce' ), //Title
//                get_permalink(wc_get_page_id('shop')) // URL
//            ];
//            array_splice($crumbs, 1, 0, [$new_breadcrumb]); //Insert a new breadcrumb after the 'Home' crumb
//        }
//        return $crumbs;
//    }, 10, 2 );

    // register add to cart action
    function dac_add_cart_button () {
        add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 10 );
    }

    add_action( 'after_setup_theme', 'dac_add_cart_button' );
