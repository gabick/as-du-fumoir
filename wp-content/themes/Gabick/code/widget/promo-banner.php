<?php
// Creating the widget
class Gabick_Promo_Banner extends WP_Widget
{

    function __construct()
    {
        parent::__construct(
            'gabick_promo_banner',     // Base ID of your widget
                    __('Gabick Promo Banner', 'gabick_promo_banner_domain'),     // Widget name will appear in UI
                    array('description' => __('Sample widget based on Gabick Promo Banner', 'gabick_promo_banner'),)     // Widget description
        );
    }

    // Creating widget front-end
    public function widget($args, $instance)
    {
        $text = $instance['text'];
        $color = apply_filters('widget_text', $instance['color']);
        $textColor = apply_filters('widget_text', $instance['textcolor']);

    // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($text)) {
            if (empty($color)) {
                $color = '#000000';
            }

            if (empty($textColor)) {
                $textColor = '#ffffff';
            }

            echo '<div class="widget widget-promo-banner" style="background-color:'.trim($color).';">';
            echo '<p class="promo-banner-text" style="color:'.$textColor.'">'.trim($text).'</p>';
            echo '</div>';
        }

    // This is where you run the code and display the output
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        if (isset($instance['text'])) {
            $text = $instance['text'];
        } else {
            $text = '';
        }

        if (isset($instance['color'])) {
            $color = $instance['color'];
        } else {
            $color = '';
        }

        if (isset($instance['textcolor'])) {
            $textColor = $instance['textcolor'];
        } else {
            $textColor = '';
        }

    // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Promo:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('text'); ?>"
                   name="<?php echo $this->get_field_name('text'); ?>" type="text"
                   value="<?php echo esc_attr($text); ?>"/>

            <label for="<?php echo $this->get_field_id('color'); ?>"><?php _e('Bar Color: Please use hexadecimal value like -> #ff0000'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('color'); ?>"
                   name="<?php echo $this->get_field_name('color'); ?>" type="text"
                   value="<?php echo esc_attr($color); ?>"/>

            <label for="<?php echo $this->get_field_id('textcolor'); ?>"><?php _e('Text Color: Please use hexadecimal value like -> #ff0000'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('textcolor'); ?>"
                   name="<?php echo $this->get_field_name('textcolor'); ?>" type="text"
                   value="<?php echo esc_attr($textColor); ?>"/>


        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['text'] = (!empty($new_instance['text'])) ? $new_instance['text'] : '';
        $instance['color'] = (!empty($new_instance['color'])) ? strip_tags($new_instance['color']) : '';
        $instance['textcolor'] = (!empty($new_instance['textcolor'])) ? ($new_instance['textcolor']) : '';
        return $instance;
    }
} // Class Gabick_social_widget ends here

// Register and load the widget
function gabick_promo_banner_widget()
{
    register_widget('Gabick_Promo_Banner');
}

add_action( 'widgets_init', 'gabick_promo_banner_widget' );


if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
            'name' => 'Promo bar Area',
            'before_widget' => '<div class = "promo-banner-area">',
            'after_widget' => '</div>',
            'id' => 'promo-bar-area'
        )
    );
}


