<?php
// Creating the widget
class Gabick_social_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(
            'gabick_social_widget',     // Base ID of your widget
                    __('Gabick Social WIdget', 'gabick_social_widget_domain'),     // Widget name will appear in UI
                    array('description' => __('Sample widget based on Gabick Social', 'gabick_social_widget_domain'),)     // Widget description
        );
    }

    // Creating widget front-end
    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

    // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo '<h4 class="title">'.$title.'</h4>';

    // This is where you run the code and display the output
//        echo __('Hello, World!', 'gabick_social_widget');
        get_template_part( 'includes/social_icons', 'gabick_social_widget' );
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Réseau Sociaux', 'gabick_social_widget');
        }
    // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // Class Gabick_social_widget ends here

// Register and load the widget
function gabick_social_load_widget()
{
    register_widget('Gabick_social_widget');
}

add_action( 'widgets_init', 'gabick_social_load_widget' );
