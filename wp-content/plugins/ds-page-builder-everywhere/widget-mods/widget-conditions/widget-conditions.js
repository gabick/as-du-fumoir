/*	This file contains code from the "Widget Visibility Without Jetpack" WordPress plugin by Joan Boluda, licensed under version 2 or higher
	of the GNU General Public License (GPL). That plugin uses code from the "Jetpack by WordPress.com" WordPress plugin by Automattic,
	licensed under version 2 or higher of the GNU General Public License (GPL). For the text of the license applicable to both of these
	source plugins, please see ../../gpl-2.0.txt.
	
	This file was modified by Aspen Grove Studios and/or Divi Space in 2018 and earlier to implement, customize, and/or maintain functionality
	for the Page Builder Everywhere widget.
	
	This file was further modified by Aspen Grove Studios:
		- 2018-12-27: Implement improvements, fixes, and changes to the widget-conditions UI
		- 2019-01-09: Fix "include children" option not saving (add conditionIndex to AJAX request)
*/

/* jshint onevar: false, smarttabs: true */
/* global ajaxurl */
/* global isRtl */

jQuery( function( $ ) {
	var widgets_shell = $( 'div#widgets-right' );

	if ( ! widgets_shell.length || ! $( widgets_shell ).find( '.widget-control-actions' ).length ) {
		widgets_shell = $( 'form#customize-controls' );
	}

	function setWidgetMargin( $widget ) {

		if ( $( 'body' ).hasClass( 'wp-customizer' ) ) {
			// set the inside widget 2 top this way we can see the widget settings
			$widget.find('.widget-inside').css( 'top', 0 );

			return;
		}

		if ( $widget.hasClass( 'expanded' ) ) {
			// The expanded widget must be at least 400px wide in order to
			// contain the visibility settings. IE wasn't handling the
			// margin-left value properly.

			if ( $widget.attr( 'style' ) ) {
				$widget.data( 'original-style', $widget.attr( 'style' ) );
			}

			var currentWidth = $widget.width();

			if ( currentWidth < 400 ) {
				var extra = 400 - currentWidth;
				if( isRtl ) {
					$widget.css( 'position', 'relative' ).css( 'right', '-' + extra + 'px' ).css( 'width', '400px' );
				} else {
					$widget.css( 'position', 'relative' ).css( 'left', '-' + extra + 'px' ).css( 'width', '400px' );
				}

			}
		}
		else if ( $widget.data( 'original-style' ) ) {
			// Restore any original inline styles when visibility is toggled off.
			$widget.attr( 'style', $widget.data( 'original-style' ) ).data( 'original-style', null );
		}
		else {
			$widget.removeAttr( 'style' );
		}
	}

	function moveWidgetVisibilityButton( $widget ) {
		var $displayOptionsButton = $widget.find( 'a.display-options' ).first();
		$displayOptionsButton.insertBefore( $widget.find( 'input.widget-control-save' ) );

		// Widgets with no configurable options don't show the Save button's container.
		$displayOptionsButton
			.parent()
				.removeClass( 'widget-control-noform' )
				.find( '.spinner' )
					.remove()
					.css( 'float', 'left' )
					.prependTo( $displayOptionsButton.parent() );
	}

	$( '.widget' ).each( function() {
		moveWidgetVisibilityButton( $( this ) );
	} );

	$( document ).on( 'widget-added', function( e, $widget ) {
		if ( $widget.find( 'div.widget-control-actions a.display-options' ).length === 0 ) {
			moveWidgetVisibilityButton( $widget );
		}
	} );

	widgets_shell.on( 'click.widgetconditions', 'a.add-condition', function( e ) {
		e.preventDefault();

		var $condition = $( this ).closest( 'div.condition' ),
			$conditionClone = $condition.clone().insertAfter( $condition );

		$conditionClone.find( 'select.conditions-rule-major' ).val( '' );
		$conditionClone.find( 'select.conditions-rule-minor' ).html( '' ).attr( 'disabled' );
		$conditionClone.find( 'span.conditions-rule-has-children' ).hide().html( '' );
		
		$condition.closest('.conditions').removeClass('ds-pbe-conditions-empty');
	} );

	widgets_shell.on( 'click.widgetconditions', 'a.display-options', function ( e ) {
		e.preventDefault();

		var $displayOptionsButton = $( this ),
			$widget = $displayOptionsButton.closest( 'div.widget' );

		$widget.find( 'div.widget-conditional' ).toggleClass( 'widget-conditional-hide' );
		$( this ).toggleClass( 'active' );
		$widget.toggleClass( 'expanded' );
		setWidgetMargin( $widget );

		if ( $( this ).hasClass( 'active' ) ) {
			$widget.find( 'input[name=widget-conditions-visible]' ).val( '1' );
		} else {
			$widget.find( 'input[name=widget-conditions-visible]' ).val( '0' );
		}

	} );

	widgets_shell.on( 'click.widgetconditions', 'a.delete-condition', function( e ) {
		e.preventDefault();

		var $condition = $( this ).closest( 'div.condition' ),
			$conditions = $condition.closest('.conditions');

		if ( $condition.is( ':first-child' ) && $condition.is( ':last-child' ) ) {
			var $ruleMajorSelect = $condition.find( 'select.conditions-rule-major' );
			if ($ruleMajorSelect.val()) {
				$ruleMajorSelect.val( '' ).change();
			}
		} else {
			var $remainingConditions = $condition.siblings('.condition');
			$condition.detach();
			$conditions.toggleClass('ds-pbe-conditions-empty', $remainingConditions.length < 2 && $remainingConditions.is('.ds-pbe-condition-empty'));
		}
	} );

	widgets_shell.on( 'click.widgetconditions', 'div.widget-top', function() {
		var $widget = $( this ).closest( 'div.widget' ),
			$displayOptionsButton = $widget.find( 'a.display-options' );

		if ( $displayOptionsButton.hasClass( 'active' ) ) {
			$displayOptionsButton.attr( 'opened', 'true' );
		}

		if ( $displayOptionsButton.attr( 'opened' ) ) {
			$displayOptionsButton.removeAttr( 'opened' );
			$widget.toggleClass( 'expanded' );
			setWidgetMargin( $widget );
		}
	} );

	$( document ).on( 'change.widgetconditions', 'select.conditions-rule-major', function() {
		var $conditionsRuleMajor = $ ( this ),
			$conditionsRuleMinor = $conditionsRuleMajor.siblings( 'select.conditions-rule-minor:first' ),
			$conditionsRuleHasChildren = $conditionsRuleMajor.siblings( 'span.conditions-rule-has-children' ),
			$condition = $conditionsRuleMajor.closest('.condition'),
			$conditions = $condition.closest('.conditions'),
			conditionsRuleMajorValue = $conditionsRuleMajor.val();
		
		$condition.toggleClass('ds-pbe-condition-empty', !conditionsRuleMajorValue);
		$conditions.toggleClass('ds-pbe-conditions-empty', $conditions.children('.condition').length < 2 && $condition.is('.ds-pbe-condition-empty'));
		
		if ( conditionsRuleMajorValue ) {
			if ( conditionsRuleMajorValue !== 'page' ){
				$conditionsRuleHasChildren.hide().html( '' );
			}

			$conditionsRuleMinor.html( '' ).append( $( '<option/>' ).text( $conditionsRuleMinor.data( 'loading-text' ) ) );

			var data = {
				action: 'widget_conditions_options',
				major: conditionsRuleMajorValue
			};

			jQuery.post( ajaxurl, data, function( html ) {
				$conditionsRuleMinor.html( html ).removeAttr( 'disabled' );
			} );
		} else {
			$conditionsRuleMajor.siblings( 'select.conditions-rule-minor' ).attr( 'disabled', 'disabled' ).html( '' );
			$conditionsRuleHasChildren.hide().html( '' );
		}
	} );

	$( document ).on( 'change.widgetconditions', 'select.conditions-rule-minor', function() {
		var $conditionsRuleMinor = $ ( this ),
			$conditionsRuleMajor = $conditionsRuleMinor.siblings( 'select.conditions-rule-major' ),
			$conditionsRuleHasChildren = $conditionsRuleMinor.siblings( 'span.conditions-rule-has-children' ),
			conditionsRuleMajorValue = $conditionsRuleMajor.val(),
			conditionsRuleMinorValue = $conditionsRuleMinor.val();

		if ( conditionsRuleMajorValue === 'page' && conditionsRuleMinorValue * 1 ) { // Exclude non-numeric values
			var data = {
				action: 'widget_conditions_has_children',
				major: conditionsRuleMajorValue,
				minor: conditionsRuleMinorValue,
				conditionIndex: $conditionsRuleMinor.attr('name')[11] == 1 ? 1 : 0
			};

			jQuery.post( ajaxurl, data, function( html ) {
				$conditionsRuleHasChildren.html( html ).show();
			} );
		} else {
			$conditionsRuleHasChildren.hide().html( '' );
		}
	} );
} );
