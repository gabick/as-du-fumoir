//-------------------------------------
//-- Site - Base
//-------------------------------------

(() => {
	'use strict';

	const local = {};


	//-- Cache data instantly
	local.cache = () => {

		// Env
		// app.env.isUniquePage = app.env.pageId === 'UID';
		// app.env.isTypePage   = _.includes(app.env.pageTags, 'TYPE-ID');
		// app.env.isOneOfThese = !!_.intersection(app.env.pageTags, ['ID1', 'ID2']).length;

	};


	//-- Cache data once DOM is loaded
	local.cacheDOM = () => {

		//

	};


	//-- Bind events once DOM is loaded
	local.bind = () => {

		//

	};


	//-- Subscribe to topics
	local.subscribe = () => {

		// pinki.message.subscribe('foo.bar',  () => {});

	};


	//-- Execute once DOM is loaded
	local.start = () => {
		local.formData();
		//
	};


	//-- Execute once page is loaded
	local.delayedStart = () => {
		//
	};

	local.contactFormSubmit = () => {
		$('#formulaire form').submit(() => {
			// dataLayer not defined
			global.dataLayer.push({
				event: 'contact-form-home'
			});
		});
	};
	local.formData = () => {
		$('.et_bloom_submit_subscription').click(() => {
			const formdiv = $('.et_bloom_form_content');
			const nameField = formdiv.find('.et_bloom_subscribe_name input').val();
			const emailField = formdiv.find('.et_bloom_subscribe_email input').val();
			if (emailField !== '') {
				$.post(
					'/newsletter-file.php',
					{
						name: nameField,
						email: emailField
					}
				).done(() => {
					formdiv.append('<div class="gabick_success_container"><p class="gabick_success_message">Merci de votre confiance, vous avez bien été ajouté a notre liste d\'envoie.</p></div>');
				});
			}

		});
	};

	// Outline
	local.cache();
	local.subscribe();

	// DOM Ready
	pinki.vow.when(DOM_PARSED).then(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});

	// Document loaded
	pinki.vow.when(DOCUMENT_LOADED).then(() => {
		local.delayedStart();
	});

})();
